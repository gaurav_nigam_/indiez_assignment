//
//  PhotoCell.h
//  IndieAssignment
//
//  Created by shaik riyaz on 29/03/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@end
