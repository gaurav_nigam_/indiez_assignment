//
//  ViewController.h
//  IndieAssignment
//
//  Created by shaik riyaz on 29/03/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DropboxSDK/DropboxSDK.h>

@interface ViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UICollectionViewDelegateFlowLayout,DBRestClientDelegate>

@end

