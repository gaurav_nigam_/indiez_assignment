//
//  ViewController.m
//  IndieAssignment
//
//  Created by shaik riyaz on 29/03/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import "ViewController.h"
#import "PhotoCell.h"
#import "AuthenticationHandler.h"
#import "FLAuthModuleViewController.h"
#import "UIAlertController+Utils.h"
#import "ImageInfoEntity+CoreDataClass.h"
#import "AppDelegate.h"
#import "Utility.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FLGalleryViewController.h"
#import <Social/Social.h>
#import "UIColor+Utils.h"

#define sectionPadding 6
#define minInerItemspacing 4
#define minLineSpacing 4


@interface ViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *photoCollectionView;

@property (strong, nonatomic) NSMutableArray<ImageInfoEntity*> *selectedImages;

@property (nonatomic, strong) AuthenticationHandler *authHandler;

@property (nonatomic, strong) NSMutableArray *picList;

@property (nonatomic, retain) DBRestClient *restClient;

@property (nonatomic,retain)  UIActivityIndicatorView *activityIndicator;

@property (nonatomic,retain) PhotoCell *selctedCell_DropBox;

@end

@implementation ViewController


- (void)viewDidLoad {

    [super viewDidLoad];
    
    self.authHandler = [AuthenticationHandler sharedAuthHandler];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userAuthenticateCallback:) name:@"UserAuthCallbackNotification" object:nil];
    
    [self.photoCollectionView registerNib:[UINib nibWithNibName:@"PhotoCell" bundle:nil] forCellWithReuseIdentifier:@"PhotoCell"];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self addflowLayout];
    
    [self fetchUserImages];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI:) name:@"ImageHasDownloaded" object:nil];
    
    [self restClient];
    
    _activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    _activityIndicator.center = self.view.center;
    
    _activityIndicator.color = [UIColor selectionColour];
    
    _activityIndicator.backgroundColor = [UIColor whiteColor];
    
    _activityIndicator.layer.cornerRadius = 8;
    
    _activityIndicator.clipsToBounds = YES;
    
    [self.view addSubview:_activityIndicator];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dropBoxauthenticationCompleted) name:@"DropBoxauthenticationCompleted" object:nil];
}

-(void)fetchUserImages {
    
    NSManagedObjectContext *context = [Utility getAppdelegateInstance].persistentContainer.viewContext;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ImageInfoEntity"];
    
    NSError *error = nil;
    
    _selectedImages = [NSMutableArray arrayWithArray:[context executeFetchRequest:request error:&error]];
}

-(void)updateUI:(NSNotification *)notif {
    
    [self fetchUserImages];
    [self.photoCollectionView reloadData];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [self.authHandler cleanUp];
    
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView // remove
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    return _selectedImages.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoCell *photoCell = [collectionView
                            dequeueReusableCellWithReuseIdentifier:@"PhotoCell"
                            forIndexPath:indexPath];
    
    ImageInfoEntity * entity = _selectedImages[indexPath.row];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];

    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:entity.imageURL];
    
    [photoCell.photoImageView sd_setImageWithURL:[NSURL fileURLWithPath:fileName]
                                placeholderImage:nil];
    
    return photoCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ImageInfoEntity *entity = _selectedImages[indexPath.row];
    
    PhotoCell *photoCell =(PhotoCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    [self displaySelectedCell:photoCell];
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Share Selected Photo to"
                                                                        message:nil
                                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [controller addAction:[UIAlertAction actionWithTitle:@"Twitter"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                     [self sharePhotoToTwitter:photoCell.photoImageView.image withCompletion:^{
                                                         [self displayDeslectedCell:photoCell];
                                                     }];
                                                     
                                                 }]];
    
    [controller addAction:[UIAlertAction actionWithTitle:@"Facebook"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                     [self sharePhotoToFacebook:photoCell.photoImageView.image withCompletion:^{
                                                         [self displayDeslectedCell:photoCell];
                                                     }];
                                                 }]];
    
    [controller addAction:[UIAlertAction actionWithTitle:@"Dropbox"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * _Nonnull action) {

                                                     _selctedCell_DropBox = photoCell;
                                                     
                                                     if (![[DBSession sharedSession] isLinked])
                                                     {
                                                         [[DBSession sharedSession] linkFromController:self];
                                                     }
                                                     else
                                                     {
                                                         [self sharePhotoToDropBox:entity photoCell:photoCell];
                                                     }

                                                 }]];
    
    [controller addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                                   style:UIAlertActionStyleCancel
                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                     
                                                     [self displayDeslectedCell:photoCell];
                                                 }]];
    
    [self presentViewController:controller animated:YES completion:nil];


}

- (IBAction)navAddButtonItemClick:(id)sender {
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Select a Photo from the following Options!"
                                                                        message:nil
                                                                 preferredStyle:UIAlertControllerStyleAlert];

    [controller addAction:[UIAlertAction actionWithTitle:@"Photo Gallery"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                     
                                                     [self openImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                                     
                                                 }]];

    [controller addAction:[UIAlertAction actionWithTitle:@"Camera"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                     
                                                     [self openImagePickerWithSourceType:UIImagePickerControllerSourceTypeCamera];
                                                 }]];
    
    [controller addAction:[UIAlertAction actionWithTitle:@"Flickr"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                     
                                                     [self.authHandler checkAuthenticationWithCompletion:^(BOOL isAuth,NSError *error) {
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             if (isAuth) {
                                                                 
                                                                 [self openFlickrGallery];
                                                             }else {
                                                                 FLAuthModuleViewController *flickrAuthView = [[FLAuthModuleViewController alloc] initWithNibName:@"FLAuthModuleViewController" bundle:nil];
                                                                 [self.navigationController pushViewController:flickrAuthView animated:YES];
                                                             }
                                                         });     
                                                     }];
                                                     
                                                 }]];
    
    [controller addCancelAction];
    
    [self presentViewController:controller animated:YES completion:nil];

}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *chosenImage =  [info valueForKey:UIImagePickerControllerOriginalImage];
    
    [self saveImage:chosenImage source:picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary?@"GALLERY":@"CAMERA"];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (IBAction)saveImage:(UIImage *) image source:(NSString *) source {
    
    NSString * fileName = [NSString stringWithFormat:@"%@.png",[Utility getTimeStampInString]];
    
    NSString *savedImagePath = [[Utility getDocumentDirectoryPath] stringByAppendingPathComponent:fileName];
    
    NSData *imageData = UIImagePNGRepresentation(image);
    
    [imageData writeToFile:savedImagePath atomically:NO];
    
    NSManagedObjectContext *context = [Utility getAppdelegateInstance].persistentContainer.viewContext;
    
    ImageInfoEntity *entity = [NSEntityDescription insertNewObjectForEntityForName:@"ImageInfoEntity" inManagedObjectContext:context];
    
    entity.imageURL = fileName;
    entity.imageSource = source;
    
    if ([context save:nil]) {
        
        [_selectedImages addObject:entity];
        
        [_photoCollectionView reloadData];
    }
}



#pragma mark - Flicker Integration Methods -

- (void) userAuthenticateCallback:(NSNotification *)notification {
    
    
    NSURL *callbackURL = notification.object;
    
    [self.authHandler completeAuthWithCallbackURL:callbackURL withCompletionHandler:^(BOOL isAuth, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                [self openFlickrGallery];
            } else {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error"
                                                                                         message:error.localizedDescription
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *camraAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault
                                                                    handler:nil];
                [alertController addAction:camraAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        });
    }];
}

#pragma mark Utility methods

-(void) openImagePickerWithSourceType:(UIImagePickerControllerSourceType) soureType {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = soureType;
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

-(void) addflowLayout
{
    
    float itemWidth = ([UIScreen mainScreen].bounds.size.width - 2*sectionPadding - 3*minInerItemspacing )/4.0;
    
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    layout.minimumLineSpacing = minLineSpacing;
    layout.minimumInteritemSpacing = minInerItemspacing;
    layout.sectionInset = UIEdgeInsetsMake(sectionPadding,sectionPadding,sectionPadding,sectionPadding);
    layout.itemSize = CGSizeMake(itemWidth, itemWidth);
    
    _photoCollectionView.collectionViewLayout = layout;
}

-(void)openFlickrGallery {
    FLGalleryViewController *flickrGalleryVC = [[FLGalleryViewController alloc] initWithNibName:@"FLGalleryViewController" bundle:nil];
    [self.navigationController pushViewController:flickrGalleryVC animated:YES];
}

-(void)displaySelectedCell:(PhotoCell *)photoCell {
    
    photoCell.layer.cornerRadius = 2;
    photoCell.layer.borderWidth = 5;
    photoCell.layer.borderColor = [UIColor selectionColour].CGColor;
}

-(void)displayDeslectedCell:(PhotoCell *)photoCell {
    
    photoCell.layer.cornerRadius = 0;
    photoCell.layer.borderWidth = 0;
    photoCell.layer.borderColor = [UIColor clearColor].CGColor;
}


-(void) sharePhotoToTwitter:(UIImage *) image withCompletion:(void(^)()) completion
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
       
        if (composeViewController) {
        
            [composeViewController addImage:image];
            
            NSString *initialTextString = @"Your Tweet";
            
            [composeViewController setInitialText:initialTextString];
            
            [composeViewController setCompletionHandler:^(SLComposeViewControllerResult result) {
            
                if (result == SLComposeViewControllerResultDone) {
                    NSLog(@"Posted");
                } else if (result == SLComposeViewControllerResultCancelled) {
                    NSLog(@"Post Cancelled");
                } else {
                    NSLog(@"Post Failed");
                }
                
                completion();
            }];
            
            [self presentViewController:composeViewController animated:YES completion:nil];
        }
    }
    else
    {
        completion();
    }

}

-(void) sharePhotoToFacebook:(UIImage *) image withCompletion:(void(^)()) completion
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [mySLComposerSheet addImage:image];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
            }
            
            completion();

        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else
    {
        completion();
    }

}

-(void) sharePhotoToDropBox:(ImageInfoEntity *) entity photoCell:(PhotoCell *)photoCell
{
    [_activityIndicator startAnimating];
    self.view.userInteractionEnabled = NO;
    
    NSString *documentsDirectory = [Utility getDocumentDirectoryPath];
    
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:entity.imageURL];
    
    NSString *destDir = @"/Photos";
    
    [self.restClient uploadFile:[fileName lastPathComponent] toPath:destDir withParentRev:nil fromPath:fileName];

}

- (void)restClient:(DBRestClient *)client
      uploadedFile:(NSString *)destPath
              from:(NSString *)srcPath
          metadata:(DBMetadata *)metadata {
    
    [_activityIndicator stopAnimating];
    
    self.view.userInteractionEnabled = YES;
    
    UIAlertView *alertSuccess = [[UIAlertView alloc]initWithTitle:@"Success" message:@"File uploaded suceessfully" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    
    [alertSuccess show];
    
    [self performSelector:@selector(dismiss:) withObject:alertSuccess afterDelay:1.0];
    
    [self displayDeslectedCell:_selctedCell_DropBox];
    
    _selctedCell_DropBox = nil;
    
    [self.restClient loadMetadata:@"/"];
    
    NSLog(@"File uploaded successfully to path: %@", metadata.path);
}

-(void)dismiss:(UIAlertView*)alert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    
}

- (void)restClient:(DBRestClient *)client uploadFileFailedWithError:(NSError *)error {
}

- (void)restClient:(DBRestClient *)client loadedMetadata:(DBMetadata *)metadata {
    
    if (metadata.isDirectory) {

        NSLog(@"Folder '%@' contains:", metadata.path);
      
        for (DBMetadata *file in metadata.contents)
        {
            NSLog(@"	%@", file.filename);
        }
    }
}

- (void)restClient:(DBRestClient *)client loadMetadataFailedWithError:(NSError *)error {
     NSLog(@"Error loading metadata: %@", error);
}

-(DBRestClient*)restClient{
    _restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
    [_restClient setDelegate:self];
    return _restClient;
}

#pragma mark Notification observers

-(void) dropBoxauthenticationCompleted
{
    NSUInteger entityIndex = [_photoCollectionView indexPathForCell:_selctedCell_DropBox].row;
    
    [self sharePhotoToDropBox:_selectedImages[entityIndex] photoCell:_selctedCell_DropBox];
}

@end
