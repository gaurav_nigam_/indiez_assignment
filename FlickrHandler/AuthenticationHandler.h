//
//  AuthenticationHandler.h
//  IndiezAssignmentDemo
//
//  Created by Gaurav Nigam on 31/03/17.
//  Copyright © 2017 DeliverIt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^IndiezAuthenticate)(BOOL isAuth, NSError *error);
typedef void (^GetUserPics)(NSMutableArray *photoList);
typedef void (^BeginAuthResponse) (NSURL *flickrLoginPageURL, NSError *error);

@interface AuthenticationHandler : NSObject

+(AuthenticationHandler *)sharedAuthHandler;
-(void)intializeFlickerKit;
-(void)initalizeDropboxSetup;
-(void)handleOpenURLForURL:(NSURL *)url;
-(void)checkAuthenticationWithCompletion:(IndiezAuthenticate)authenticate;
-(void)getAllPicsOfUserWithController:(UIViewController *)controller completionHandler:(GetUserPics)userPics;
-(void)completeAuthWithCallbackURL:(NSURL *)url withCompletionHandler:(IndiezAuthenticate)authStatus;
-(void)beginAuthWithCallBackURL:(NSString *)url withCompletionHandler:(BeginAuthResponse)authResponse;
-(void)cancelAuthOp;
-(void)cleanUp;
-(NSMutableArray *)getDownloadedPics;
-(BOOL)isMorePicAvailableToShow;

@end
