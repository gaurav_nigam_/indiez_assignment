//
//  FLAuthModuleViewController.m
//  IndiezAssignmentDemo
//
//  Created by Gaurav Nigam on 31/03/17.
//  Copyright © 2017 DeliverIt. All rights reserved.
//

#import "FLAuthModuleViewController.h"
#import "FlickrKit.h"
#import "AuthenticationHandler.h"

#define CALLBACK_URL @"indiezassignmentdemo://auth"

@interface FLAuthModuleViewController ()
@property (nonatomic, retain) FKDUNetworkOperation *authOp;
@end

@implementation FLAuthModuleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Flickr Auth";
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Begin the authentication process
    [[AuthenticationHandler sharedAuthHandler] beginAuthWithCallBackURL:CALLBACK_URL withCompletionHandler:^(NSURL *flickrLoginPageURL, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:flickrLoginPageURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
                [self.webView loadRequest:urlRequest];
            } else {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error"
                                                                                         message:error.localizedDescription
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *camraAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault
                                                                    handler:nil];
                [alertController addAction:camraAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        });
    }];
}

- (void) viewWillDisappear:(BOOL)animated {
    [[AuthenticationHandler sharedAuthHandler] cancelAuthOp];
    [super viewWillDisappear:animated];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    //If they click NO DONT AUTHORIZE, this is where it takes you by default... maybe take them to my own web site, or show something else
    
    NSURL *url = [request URL];
    
    // If it's the callback url, then lets trigger that
    if (![url.scheme isEqual:@"http"] && ![url.scheme isEqual:@"https"]) {
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
            return NO;
        }
    }
    
    return YES;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
