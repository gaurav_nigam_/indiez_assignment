//
//  FLAuthModuleViewController.h
//  IndiezAssignmentDemo
//
//  Created by Gaurav Nigam on 31/03/17.
//  Copyright © 2017 DeliverIt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLAuthModuleViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end
