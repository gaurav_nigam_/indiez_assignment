//
//  FLGalleryViewController.h
//  IndieAssignment
//
//  Created by Gaurav Nigam on 02/04/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLGalleryViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *picCollectionView;

@end
