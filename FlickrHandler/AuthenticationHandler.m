//
//  AuthenticationHandler.m
//  IndiezAssignmentDemo
//
//  Created by Gaurav Nigam on 31/03/17.
//  Copyright © 2017 DeliverIt. All rights reserved.
//

#import "AuthenticationHandler.h"
#import "FlickrKit.h"
#import "FLPhoto.h"
#import <DropboxSDK/DropboxSDK.h>

#define FLICKR_API_KEY @"2b2bfd7f1dabba30749d50951088d7c1"
#define FLICKR_SECRET @"c1c63603c92dcb2f"
#define FLICKR_SCHEME @"indiezassignmentdemo"
#define DROPBOX_APP_KEY @"ld8acjlqwrp6285"
#define DROPBOX_APP_SECRET @"7m5t2iub24df6em"

@interface AuthenticationHandler ()
@property (nonatomic, retain) FKDUNetworkOperation *checkAuthOp;
@property (nonatomic, retain) FKFlickrNetworkOperation *myPhotostreamOp;
@property (nonatomic, retain) FKDUNetworkOperation *completeAuthOp;
@property (nonatomic, retain) FKDUNetworkOperation *authOp;
@property (nonatomic, retain) NSString *userID;
@property (nonatomic, strong) NSMutableArray *userPics;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic, assign) NSInteger page;

@end

@implementation AuthenticationHandler

+(AuthenticationHandler *)sharedAuthHandler {
    static AuthenticationHandler *authHandler = nil;
    static dispatch_once_t token;
    
    dispatch_once(&token, ^{
        authHandler = [[AuthenticationHandler alloc] init];
    });
    
    return authHandler;
    
}

-(instancetype)init {
    self = [super init];
    if (self) {
        self.userPics = [[NSMutableArray alloc] init];
        self.page = -1;
        self.totalPages = 0;
    }
    return self;
}

-(void)intializeFlickerKit {
    
    [[FlickrKit sharedFlickrKit] initializeWithAPIKey:FLICKR_API_KEY sharedSecret:FLICKR_SECRET];
}

-(void)initalizeDropboxSetup {
    
    DBSession *dbSession = [[DBSession alloc]
                            initWithAppKey:DROPBOX_APP_KEY
                            appSecret:DROPBOX_APP_SECRET
                            root:kDBRootAppFolder];
    [DBSession setSharedSession:dbSession];
}

-(void)handleOpenURLForURL:(NSURL *)url {
    
    NSString *scheme = [url scheme];
    
    if([FLICKR_SCHEME isEqualToString:scheme]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UserAuthCallbackNotification" object:url userInfo:nil];
    }else {
        if ([[DBSession sharedSession] handleOpenURL:url]){
            if ([[DBSession sharedSession] isLinked]) {
                NSLog(@"App linked successfully!");
                // At this point you can start making API calls
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DropBoxauthenticationCompleted" object:nil];
            }
        }
    }
}

-(void)checkAuthenticationWithCompletion:(IndiezAuthenticate)authenticate {
    
    self.checkAuthOp = [[FlickrKit sharedFlickrKit] checkAuthorizationOnCompletion:^(NSString *userName, NSString *userId, NSString *fullName, NSError *error) {
        if (!error) {
            self.userID = userId;
            authenticate(YES, nil);
        } else {
            authenticate(NO, error);
        }
    }];
}

-(void)getAllPicsOfUserWithController:(UIViewController *)controller completionHandler:(GetUserPics)userPics {
    
    if ([FlickrKit sharedFlickrKit].isAuthorized) {
        if (self.page < self.totalPages) {
            if (self.page == -1) {
                self.page = 0;
            }
            self.page++;
            self.myPhotostreamOp = [[FlickrKit sharedFlickrKit] call:@"flickr.photos.search" args:@{@"user_id": self.userID, @"per_page": @"35", @"page":[NSString stringWithFormat:@"%ld",(long)self.page]} maxCacheAge:FKDUMaxAgeNeverCache completion:^(NSDictionary *response, NSError *error) {
                
                if (response) {
                    NSMutableArray *photoURLs = [NSMutableArray array];
                    self.totalPages = [[response valueForKeyPath:@"photos.pages"] integerValue];
                    self.page = [[response valueForKeyPath:@"photos.page"] integerValue];
                    
                    for (NSDictionary *photoDictionary in [response valueForKeyPath:@"photos.photo"]) {
                        NSURL *url = [[FlickrKit sharedFlickrKit] photoURLForSize:FKPhotoSizeSmall240 fromPhotoDictionary:photoDictionary];
                        
                        FLPhoto *pic = [[FLPhoto alloc] init];
                        pic.picURL = url;
                        pic.picId = photoDictionary[@"id"];
                        [photoURLs addObject:pic];
                    }
                    
                    [self.userPics addObjectsFromArray:photoURLs];
                    userPics(self.userPics);
                    
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController *alert = [self showAlertViewWithTitle:@"Error" message:error.localizedDescription cancelButtonTitle:@"OK"];
                        [controller presentViewController:alert animated:YES completion:nil];
                    });
                }
                
            }];
        }
        
    } else {
        
        UIAlertController *alert = [self showAlertViewWithTitle:@"Error" message:@"Please login first" cancelButtonTitle:@"OK"];
        [controller presentViewController:alert animated:YES completion:nil];
    }
}

-(UIAlertController *)showAlertViewWithTitle:(NSString *)title message:(NSString *)msg cancelButtonTitle:(NSString *)btnTitle {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:msg
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *camraAction = [UIAlertAction actionWithTitle:btnTitle
                                                          style:UIAlertActionStyleDefault
                                                        handler:nil]; 
    [alertController addAction:camraAction];
    
    return alertController;
}

-(void)completeAuthWithCallbackURL:(NSURL *)url withCompletionHandler:(IndiezAuthenticate)authStatus {
    
    self.completeAuthOp = [[FlickrKit sharedFlickrKit] completeAuthWithURL:url completion:^(NSString *userName, NSString *userId, NSString *fullName, NSError *error) {
        if (!error) {
            self.userID = userId;
            authStatus(YES, nil);
        } else {
            authStatus(NO, error);
        }
    }];
}

-(void)beginAuthWithCallBackURL:(NSString *)url withCompletionHandler:(BeginAuthResponse)authResponse {
    self.authOp = [[FlickrKit sharedFlickrKit] beginAuthWithCallbackURL:[NSURL URLWithString:url] permission:FKPermissionDelete completion:^(NSURL *flickrLoginPageURL, NSError *error) {
        authResponse(flickrLoginPageURL, error);
    }];
}

-(void)cleanUp {
    [self.checkAuthOp cancel];
    [self.completeAuthOp cancel];
}
-(void)cancelAuthOp {
    [self.authOp cancel];
}

-(BOOL)isMorePicAvailableToShow {
    
    if (self.page == self.totalPages) {
        return NO;
    }
    
    return YES;
}

-(NSMutableArray *)getDownloadedPics {
    return self.userPics;
}

@end
