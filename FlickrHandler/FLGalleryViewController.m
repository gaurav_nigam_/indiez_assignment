//
//  FLGalleryViewController.m
//  IndieAssignment
//
//  Created by Gaurav Nigam on 02/04/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import "FLGalleryViewController.h"
#import "PhotoCell.h"
#import "AuthenticationHandler.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "IndicatorCell.h"
#import "FLPhoto.h"
#import "Utility.h"
#import "ImageDownloadInfo+CoreDataClass.h"
#import "ImageDownloadManager.h"
#import "UIColor+Utils.h"

#define sectionPadding 6
#define minInerItemspacing 4
#define minLineSpacing 4
#define INDICATOR_CELL 100

@interface FLGalleryViewController ()
@property (strong, nonatomic) NSMutableArray<FLPhoto*> *flickrImages;
@property (strong, nonatomic) AuthenticationHandler *authHandler;
@property (strong, nonatomic) UIBarButtonItem *saveBtn;
@end

@implementation FLGalleryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.picCollectionView registerNib:[UINib nibWithNibName:@"PhotoCell" bundle:nil] forCellWithReuseIdentifier:@"PhotoCell"];
    
    [self.picCollectionView registerNib:[UINib nibWithNibName:@"IndicatorCell" bundle:nil] forCellWithReuseIdentifier:@"IndicatorCell"];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self addflowLayout];
    
    self.authHandler = [AuthenticationHandler sharedAuthHandler];
    self.flickrImages = [self.authHandler getDownloadedPics];
    
    if (self.flickrImages.count == 0) {
        [self getUserPics];
    }
    
    self.saveBtn = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(savePics:)];
    self.navigationItem.rightBarButtonItem = self.saveBtn;
    
    // Do any additional setup after loading the view from its nib.
}

-(void)saveOptionVisibility {
    
    NSArray *selectedPics = [self selectedUserPics];
    if (selectedPics.count>0) {
        self.saveBtn.enabled = YES;
    }else {
        self.saveBtn.enabled = NO;
    }
}

-(NSArray *)selectedUserPics {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSelected == YES"];
    
    NSArray *selectedPics = [self.flickrImages filteredArrayUsingPredicate:predicate];
    
    return selectedPics;
}

-(void)getUserPics {
    
    [self.authHandler getAllPicsOfUserWithController:self completionHandler:^(NSMutableArray *photoList) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.flickrImages = photoList;
            NSLog(@"PicCount %ld",(long)self.flickrImages.count);
            [self.picCollectionView reloadData];
        });
    }];
}

#pragma mark Collection View Delegate Methods 

-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    if([self.authHandler isMorePicAvailableToShow])
        return ([self.flickrImages count] + 1);
    else
        return [self.flickrImages count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.flickrImages.count) {
        PhotoCell *photoCell = [collectionView
                                dequeueReusableCellWithReuseIdentifier:@"PhotoCell"
                                forIndexPath:indexPath];
        FLPhoto *pic = _flickrImages[indexPath.row];
        
        if (pic.isSelected) {
            
            [self displaySelectedCell:photoCell];
        }else {
            [self displayDeslectedCell:photoCell];
        }
        
        [photoCell.photoImageView sd_setImageWithURL:pic.picURL
                                    placeholderImage:nil];
        
        return photoCell;
    }
    
    IndicatorCell * indicatorCell = [collectionView
                                     dequeueReusableCellWithReuseIdentifier:@"IndicatorCell"
                                     forIndexPath:indexPath];
    indicatorCell.tag = INDICATOR_CELL;
    return indicatorCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    id cell = [collectionView cellForItemAtIndexPath:indexPath];
    if (![cell isKindOfClass:[IndicatorCell class]] && ((IndicatorCell*)cell).tag != INDICATOR_CELL) {
        
        FLPhoto *pic = [self.flickrImages objectAtIndex:indexPath.row];
        PhotoCell *photoCell =(PhotoCell *)cell;
        if (!pic.isSelected) {
            pic.isSelected = YES;
            
            [self displaySelectedCell:photoCell];
        }else {
            
            pic.isSelected = NO;
            
            [self displayDeslectedCell:photoCell];
        }
        
        [self saveOptionVisibility];
    }
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(cell.tag == INDICATOR_CELL) {
        
        [self getUserPics];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) addflowLayout
{
    
    float itemWidth = ([UIScreen mainScreen].bounds.size.width - 2*sectionPadding - 3*minInerItemspacing )/4.0;
    
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    layout.minimumLineSpacing = minLineSpacing;
    layout.minimumInteritemSpacing = minInerItemspacing;
    layout.sectionInset = UIEdgeInsetsMake(sectionPadding,sectionPadding,sectionPadding,sectionPadding);
    layout.itemSize = CGSizeMake(itemWidth, itemWidth);
    
    _picCollectionView.collectionViewLayout = layout;
}

-(void)displaySelectedCell:(PhotoCell *)photoCell {
    
    photoCell.layer.cornerRadius = 2;
    photoCell.layer.borderWidth = 5;
    photoCell.layer.borderColor = [UIColor selectionColour].CGColor;
}

-(void)displayDeslectedCell:(PhotoCell *)photoCell {
    
    photoCell.layer.cornerRadius = 0;
    photoCell.layer.borderWidth = 0;
    photoCell.layer.borderColor = [UIColor clearColor].CGColor;
}

#pragma mark - Saving pics -

-(void)savePics:(id)sender {
    
    NSArray *needToSavePics = [self selectedUserPics];
    
    NSManagedObjectContext *context = [Utility getAppdelegateInstance].persistentContainer.viewContext;
    for (FLPhoto *photo in needToSavePics) {
        
        if ([[ImageDownloadManager sharedDownloadManager] getImageDownloadInfoWithPicURL:[photo.picURL absoluteString]]) {
            continue;
        }
        ImageDownloadInfo *entity = [NSEntityDescription insertNewObjectForEntityForName:@"ImageDownloadInfo" inManagedObjectContext:context];
        
        entity.picid = photo.picId;
        entity.picurl = [NSString stringWithFormat:@"%@",photo.picURL];
        entity.isDownloaded = NO;
        
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Error :%@",error.description);
        }
    }
    
    [[ImageDownloadManager sharedDownloadManager] startDownload];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
