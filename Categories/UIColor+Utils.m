//
//  UIColor+Utils.m
//  IndieAssignment
//
//  Created by shaik riyaz on 05/04/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import "UIColor+Utils.h"

@implementation UIColor (Utils)

+(UIColor *)selectionColour
{
    return [UIColor colorWithRed:241.0/255 green:151.0/255 blue:72.0/255 alpha:1];
}

@end
