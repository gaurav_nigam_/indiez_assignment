//
//  UIAlertController+Utils.h
//  IndieAssignment
//
//  Created by shaik riyaz on 02/04/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Utils)

-(void) addCancelAction;

@end
