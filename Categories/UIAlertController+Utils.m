//
//  UIAlertController+Utils.m
//  IndieAssignment
//
//  Created by shaik riyaz on 02/04/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import "UIAlertController+Utils.h"

@implementation UIAlertController (Utils)

-(void)addCancelAction
{
    [self addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
}

@end
