//
//  UIColor+Utils.h
//  IndieAssignment
//
//  Created by shaik riyaz on 05/04/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Utils)

+(UIColor *) selectionColour;

@end
