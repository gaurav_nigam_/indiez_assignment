//
//  Utility.m
//  IndieAssignment
//
//  Created by shaik riyaz on 02/04/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import "Utility.h"

@implementation Utility

+(AppDelegate *)getAppdelegateInstance
{
    return (AppDelegate *)[[UIApplication sharedApplication]delegate];
}

+(NSString *)getDocumentDirectoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

+(NSString *)getTimeStampInString
{
    NSDate *date = [NSDate date];
    return [NSString stringWithFormat:@"%f",[date timeIntervalSince1970]];
}

@end
