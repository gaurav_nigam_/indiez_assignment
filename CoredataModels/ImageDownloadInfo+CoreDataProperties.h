//
//  ImageDownloadInfo+CoreDataProperties.h
//  
//
//  Created by Gaurav Nigam on 03/04/17.
//
//  This file was automatically generated and should not be edited.
//

#import "ImageDownloadInfo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ImageDownloadInfo (CoreDataProperties)

+ (NSFetchRequest<ImageDownloadInfo *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *picid;
@property (nullable, nonatomic, copy) NSString *picurl;
@property (nonatomic) BOOL isDownloaded;

@end

NS_ASSUME_NONNULL_END
