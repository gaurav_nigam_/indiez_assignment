//
//  ImageDownloadInfo+CoreDataClass.h
//  
//
//  Created by Gaurav Nigam on 03/04/17.
//
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageDownloadInfo : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ImageDownloadInfo+CoreDataProperties.h"
