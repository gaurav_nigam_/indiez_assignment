//
//  ImageDownloadInfo+CoreDataProperties.m
//  
//
//  Created by Gaurav Nigam on 03/04/17.
//
//  This file was automatically generated and should not be edited.
//

#import "ImageDownloadInfo+CoreDataProperties.h"

@implementation ImageDownloadInfo (CoreDataProperties)

+ (NSFetchRequest<ImageDownloadInfo *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ImageDownloadInfo"];
}

@dynamic picid;
@dynamic picurl;
@dynamic isDownloaded;

@end
