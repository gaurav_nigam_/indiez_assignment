//
//  FLPhoto.h
//  IndieAssignment
//
//  Created by Gaurav Nigam on 02/04/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLPhoto : NSObject
@property (nonatomic, strong) NSURL *picURL;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, strong) NSString *picId;
@end
