//
//  ImageDownloadManager.h
//  IndieAssignment
//
//  Created by Gaurav Nigam on 03/04/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^NUBackgroundSessionCompletionHandler)();

@class ImageDownloadInfo;
@interface ImageDownloadManager : NSObject

+(ImageDownloadManager *)sharedDownloadManager;
-(void)startDownload;
- (void)setBackgroundSessionCompletionHandlerBlock:(NUBackgroundSessionCompletionHandler)aBackgroundSessionCompletionHandlerBlock;
-(ImageDownloadInfo *)getImageDownloadInfoWithPicURL:(NSString *)picURL;
@end
