//
//  ImageDownload.h
//  IndieAssignment
//
//  Created by Gaurav Nigam on 03/04/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageDownload : NSObject

@property (nonatomic, strong) NSString *url;
@property (nonatomic, assign) BOOL isDownloading;
@property (nonatomic, strong) NSURLSessionDownloadTask *downloadTask;
@end
