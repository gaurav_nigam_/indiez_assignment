//
//  ImageDownloadManager.m
//  IndieAssignment
//
//  Created by Gaurav Nigam on 03/04/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import "ImageDownloadManager.h"
#import "Utility.h"
#import "ImageDownloadInfo+CoreDataClass.h"
#import "ImageDownload.h"
#import "ImageInfoEntity+CoreDataClass.h"

#define BACKGROUND_SESSION_IDENTIFIER @"bgSessionConfiguration"

@interface ImageDownloadManager ()<NSURLSessionDelegate,NSURLSessionDownloadDelegate>
@property (nonatomic, strong)NSURLSession *downloadSession;
@property (nonatomic, strong)NSMutableDictionary *activeDownloads;
@property (nonatomic, copy, nullable) NUBackgroundSessionCompletionHandler bgSessionCompletionHandlerBlock;
@end

@implementation ImageDownloadManager

+(ImageDownloadManager *)sharedDownloadManager {
    
    static ImageDownloadManager *downloadManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        downloadManager = [[ImageDownloadManager alloc] init];
    });
    
    return downloadManager;
}

-(instancetype)init {
    self = [super init];
    
    if (self) {
        [self configureSession];
        self.activeDownloads = [NSMutableDictionary dictionary];
    }
    return self;
}

-(void)configureSession {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:BACKGROUND_SESSION_IDENTIFIER];
    
    self.downloadSession = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
}

-(void)cleanUp {
    
    NSManagedObjectContext *context = [Utility getAppdelegateInstance].persistentContainer.viewContext;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ImageDownloadInfo"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDownloaded == YES"];
    
    [request setPredicate:predicate];
    
    
    NSError *error = nil;
    
    NSArray *images = [context executeFetchRequest:request error:&error];
    
    if (!error && images.count > 0) {
        for(ImageDownloadInfo *downloadInfo in images){
            [context deleteObject:downloadInfo];
        }
        
        [context save:nil];
    }
}

- (void)setBackgroundSessionCompletionHandlerBlock:(nullable NUBackgroundSessionCompletionHandler)aBackgroundSessionCompletionHandlerBlock
{
    self.bgSessionCompletionHandlerBlock = aBackgroundSessionCompletionHandlerBlock;
}


-(void)startDownload {
    
    [self cleanUp];
    
    NSManagedObjectContext *context = [Utility getAppdelegateInstance].persistentContainer.viewContext;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ImageDownloadInfo"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDownloaded == NO"];
    
    [request setPredicate:predicate];
    
    
    NSError *error = nil;
    
    NSArray *images = [context executeFetchRequest:request error:&error];
    
    for (ImageDownloadInfo *info in images) {
        ImageDownload *download = [[ImageDownload alloc] init];
        download.downloadTask = [self.downloadSession downloadTaskWithURL:[NSURL URLWithString:info.picurl]];
        download.isDownloading = YES;
        download.url = info.picurl;
        self.activeDownloads[download.url] = download;
        
        [download.downloadTask resume];
    }
    
}

#pragma mark - NSURLSessionDelegate methods -

-(void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session{
    
    if (self.bgSessionCompletionHandlerBlock) {
        void (^completionHandler)() = self.bgSessionCompletionHandlerBlock;
        self.bgSessionCompletionHandlerBlock = nil;
        completionHandler();
    }
    
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    
    NSString *originalURL = [downloadTask.originalRequest.URL absoluteString];
    if (originalURL) {
        
        NSString * fileName = [NSString stringWithFormat:@"%@.png",[Utility getTimeStampInString]];
        
        NSString *savedImagePath = [[Utility getDocumentDirectoryPath] stringByAppendingPathComponent:fileName];
        
        NSURL *localURL = [NSURL fileURLWithPath:savedImagePath];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        [fileManager removeItemAtURL:localURL error:nil];
        
        [fileManager copyItemAtURL:location toURL:localURL error:nil];
        
        self.activeDownloads[originalURL] = nil;
        
        NSManagedObjectContext *context = [Utility getAppdelegateInstance].persistentContainer.viewContext;
        
        ImageInfoEntity *entity = [NSEntityDescription insertNewObjectForEntityForName:@"ImageInfoEntity" inManagedObjectContext:context];
        
        entity.imageURL = fileName;
        entity.imageSource = @"FLICKR";
    
        if ([context save:nil]) {
            
            ImageDownloadInfo *info = [self getImageDownloadInfoWithPicURL:[downloadTask.originalRequest.URL absoluteString]];
            
            [self updateInfo:info];
            [self deleteInfo:info];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ImageHasDownloaded" object:self];
            });
        }
        
    }
}

-(ImageDownloadInfo *)getImageDownloadInfoWithPicURL:(NSString *)picURL {
    
    NSManagedObjectContext *context = [Utility getAppdelegateInstance].persistentContainer.viewContext;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ImageDownloadInfo"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDownloaded == NO AND picurl == %@",picURL];
    
    [request setPredicate:predicate];
    
    
    NSError *error = nil;
    
    NSArray *images = [context executeFetchRequest:request error:&error];
    
    return [images firstObject];
    
}

-(void)updateInfo:(ImageDownloadInfo *)info {
    
    NSManagedObjectContext *context = [Utility getAppdelegateInstance].persistentContainer.viewContext;
    
    info.isDownloaded = YES;
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"%@",error.description);
    }
}

-(void)deleteInfo:(ImageDownloadInfo *)info {
    
    NSManagedObjectContext *context = [Utility getAppdelegateInstance].persistentContainer.viewContext;
    
    [context deleteObject:info];
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"%@",error.description);
    }
}

@end
