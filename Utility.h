//
//  Utility.h
//  IndieAssignment
//
//  Created by shaik riyaz on 02/04/17.
//  Copyright © 2017 SELF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface Utility : NSObject

+(AppDelegate *) getAppdelegateInstance;
+(NSString *)getDocumentDirectoryPath;
+(NSString *)getTimeStampInString;

@end
